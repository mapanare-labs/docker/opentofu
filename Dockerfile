FROM alpine:3.21

LABEL maintainer="Enmanuel Moreira <enmanuelmoreira@gmail.com>"

ENV OPENTOFU_VERSION="1.9.0"

RUN apk add --update --no-cache bash git openssh unzip

# Determine the target architecture using uname -m
RUN case `uname -m` in \
    x86_64) ARCH=amd64; ;; \
    aarch64) ARCH=arm64; ;; \
    *) echo "Unsupported architecture, exiting..."; exit 1; ;; \
    esac \
    && OPENTOFU_URL="https://github.com/opentofu/opentofu/releases/download/v${OPENTOFU_VERSION}/tofu_${OPENTOFU_VERSION}_linux_${ARCH}.zip" \
    && wget -q "${OPENTOFU_URL}" -O /tmp/tofu_${OPENTOFU_VERSION}_linux_${ARCH}.zip \
    && unzip /tmp/tofu_${OPENTOFU_VERSION}_linux_${ARCH}.zip \
    && mv tofu /usr/bin/ \
    && chmod +x /usr/bin/tofu \
    && rm -rf /tmp/* \
    && apk del unzip

ENTRYPOINT ["/usr/bin/tofu"]